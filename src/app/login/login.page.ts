import { Component } from '@angular/core';
import { LoadingController , NavController , ToastController} from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  login = {
    "name" : "",
    "pass" : ""
  }
  constructor(
    public loadCtrl : LoadingController,
    public http : HttpClient,
    public navCtrl : NavController,
    public storage : Storage,
    public toast : ToastController
  ) { }

  async adminLogin(){
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    
    let load = await this.loadCtrl.create({
      spinner: 'crescent',
      message: 'Please wait a moment'
    })

    load.present();

    this.http.get('https://otomax.store/driver/mobile/apps.php?req=userLogin&param='+this.login.name+'&param2='+this.login.pass
    ,{headers}).pipe(map((res: any) => res)).subscribe(data => {
      console.log(data)
      if(data == null){
        load.dismiss();
      }else{
        if(data.status == 'error'){
          this.presentToast();
        }else if (data.user.description == 'admin'){
          this.storage.set('login_id',data.user.username);
          this.navCtrl.navigateRoot('home');
        }
        load.dismiss();
      }
    });
  }

  async presentToast(){
    const toast = await this.toast.create({
      message: 'Username/Password Salah',
      duration: 2000,
    })
    toast.present();
  }

}
