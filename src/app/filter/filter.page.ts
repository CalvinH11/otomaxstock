import { Component } from '@angular/core';
import { ModalController, LoadingController, NavParams } from '@ionic/angular';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.page.html',
  styleUrls: ['./filter.page.scss'],
})
export class FilterPage {
  selectedIndex: number;
  attrList: any;
  isLoading: any;

  diameterArr: any;
  etArr: any;
  spesifikasiArr: any;
  tireSpekArr: any;
  widthArr: any;

  diameterList: any;
  etList: any;
  spesifikasiList: any;
  tireSpekList: any;
  widthList: any;

  selectedDiameter: any;
  selectedEt: any;
  selectedSpek: any;
  selectedTireSpek: any;
  selectedWidth: any;

  quantityList: any;
  selectedQtyVal: any;

  constructor(
    public modalCtrl : ModalController, 
    private apiService : ApiService,
    public loadCtrl : LoadingController,
    public param : NavParams
    ) {
    this.quantityList = [
      {'name' : 'Kosong', 'value' : 'kosong'},
      {'name' : 'Ganjil', 'value' : 'ganjil'},
      {'name' : 'Ready', 'value' : 'ready'}
    ]

    this.presentLoad();
    this.getAttr();
    this.gettingInfo(param);
    console.log(param)

  }

  async getAttr(){
    await this.apiService.getAllAttribute().then((res) => {
      this.diameterList = [res[2]];
      this.etList = [res[3]];
      this.spesifikasiList = [res[5]];
      this.tireSpekList = [res[6]];
      this.widthList = [res[7]];
      this.attrList = res;
    })
    if(this.attrList){
      this.dismissLoad();
    }
  }
  
  gettingInfo(param){
    this.selectedDiameter = param.data.diameterVal.id;
    this.selectedEt = param.data.etVal.id;
    this.selectedSpek = param.data.spekVal.id;
    this.selectedTireSpek = param.data.tireSpekVal.id;
    this.selectedWidth = param.data.widthVal.id;
    this.selectedQtyVal = param.data.qtyVal;

    this.diameterArr = param.data.diameterVal ? {'slug': param.data.diameterVal.slug, 'id': param.data.diameterVal.id} : undefined;
    this.etArr = param.data.etVal ? {'slug': param.data.etVal.slug, 'id': param.data.etVal.id} : undefined;
    this.spesifikasiArr = param.data.spekVal ? {'slug' : param.data.spekVal.slug, 'id': param.data.spekVal.id} : undefined;
    this.tireSpekArr = param.data.tireSpekVal ? {'slug': param.data.tireSpeVal.slug, 'id': param.data.tireSpekVal.id} : undefined;
    this.widthArr = param.data.widthVal ? {'slug': param.data.widthVal.slug, 'id': param.data.widthVal.id} : undefined;
  }

  filterAttr(slug: string, id: string = null){
    if(slug == 'pa_diameter'){
      this.selectedDiameter = id;
      this.diameterArr = {'slug' : slug,'id' : id};
    }else if (slug == 'pa_et'){
      this.selectedEt = id;
      this.etArr = {'slug' : slug,'id' : id};
    }else if(slug == 'pa_spesifikasi'){
      this.selectedSpek = id;
      this.spesifikasiArr = {'slug' : slug,'id' : id};
    }else if(slug == 'pa_tire-spesifikasi'){
      this.selectedTireSpek = id;
      this.tireSpekArr = {'slug' : slug,'id' : id};
    }else if(slug == 'pa_width'){
      this.selectedWidth = id;
      this.widthArr = {'slug' : slug,'id' : id};
    }
    console.log(this.diameterArr,'diameter arr')
  }

  filterQty(val: string){
    this.selectedQtyVal = val;
  }

  async dismiss() {
    await this.modalCtrl.dismiss();
  }

  async submitModal(){
    await this.modalCtrl.dismiss({
      'attributeList' : this.attrList,
      'filterDiameter': this.diameterArr,
      'filterEt': this.etArr,
      'filterSpesifikasi': this.spesifikasiArr,
      'filterTireSpek': this.tireSpekArr,
      'filterWidth': this.widthArr,
      'filterQty' : this.selectedQtyVal
    });
  }

  async presentLoad(){
    this.isLoading = true;
    const load = await this.loadCtrl.create({
      spinner: 'dots',
      duration: 20000,
      message: 'Please Wait a Moment'
    }).then((load) => {
      load.present().then(() => {
        if(!this.isLoading) load.dismiss();
      })
    })
  }

  async dismissLoad(){
    this.isLoading = false;
    return await this.loadCtrl.dismiss();
  }

}
