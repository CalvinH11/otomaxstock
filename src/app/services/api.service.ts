import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  APILink = 'https://otomax.store/api/mobile/apps.php';
  productList: any;
  attributeList: any;
  constructor( 
    private http : HttpClient
    ) { }

    

  getProductList(page: number, ctg_id: string = null, attr_id: string = null, search: string = null) {
    return new Promise(resolve => {
      let pageParam = '', searchParam = '', categoryParam = '', attributeParam = '';
      if (page != null && page > 0) { pageParam = '&page=' + page; }
      if (!(ctg_id == null || ctg_id === '')) { categoryParam = '&param=' + ctg_id; }
      if (!(attr_id == null || attr_id === '')) { attributeParam = '&param2=' + attr_id; }
      if (!(search == null || search === '')) { searchParam = '&search=' + search; }

      if (this.productList == null) { this.productList = []; }
      this.http.get(this.APILink + '?req=getListProduct' + pageParam + categoryParam + attributeParam + searchParam).subscribe((data) => {
        this.productList[page - 1] = data;
        resolve(data);
      });
    });
  }

  getAttributeList(page: number = null, parent: string = null) {
    return new Promise(resolve => {
      let parentParam = '', pageParam = '';
      if (page != null && page > 0) { pageParam = '&page=' + page; }
      if (!(parent == null || parent === '')) { parentParam = '&uid=' + parent; }
      this.http.get(this.APILink + '?req=getAttributeList' + parentParam + pageParam).subscribe((data) => {
        resolve(data);
      });
    });
  }

  async getAllAttribute() {
    if (this.attributeList == null) {
      this.attributeList = [];
      let bol = true;
      let parentList = [];
      const data = await this.getAttributeList();
      // @ts-ignore
      parentList = [...parentList, ...data];
      // @ts-ignore
      if (data.length === 0) { bol = false; }

      // tslint:disable-next-line:prefer-for-of
      for (let x = 0; x < parentList.length; x++) {
        const parent = parentList[x];
        bol = true;
        let childList = [];
        for (let y = 1; bol; y++) {
          // tslint:disable-next-line:no-shadowed-variable
          const data: any  = await this.getAttributeList(y, parent.id);
          // @ts-ignore
          childList = [...childList, ...data];
          // @ts-ignore
          if (data.length === 0) { bol = false; }
        }

        this.attributeList.push({
          parent,
          children: childList,
          filter: null
        });
      }
      console.log(this.attributeList);
    }
    return this.attributeList;
  }

  authUser(username: string, password: string, sec: number = null) {
    let secondsParam = '';
    if (sec != null && sec > 0) { secondsParam = '&val=' + sec; }

    return new Promise(resolve => {
      this.http.get(this.APILink + '?req=authUser&uid=' + username + '&param=' + password + secondsParam).subscribe(
        (data) => {
          console.log(data);
          resolve(data);
        },
        (error) => {
          console.log(error);
          resolve(error);
        }
      );
    });
  }
}
