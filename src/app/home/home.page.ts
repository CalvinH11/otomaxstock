import { Component , ViewChild } from '@angular/core';
import { ApiService } from '../services/api.service';
import { IonInfiniteScroll, ModalController, MenuController } from '@ionic/angular';
import { FilterPage } from '../filter/filter.page';
import { DetailPage } from '../detail/detail.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage  {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  listProduk: any;
  listProdukBackup: any;
  page : number;
  search: any;
  filterStock : any;
  dataFilter: any;
  ctgId: any;
  attrString: any;

  diameterArr: any;
  etArr: any;
  spesifikasiArr: any;
  tireSpekArr: any;
  widthArr: any;

  idFilter: any;

  constructor(
    private apiService : ApiService,
    public modalCtrl : ModalController,
    private menu: MenuController,
  ) {
    this.filterStock = null;
    this.reload();
  }


  reload(){
    this.listProduk = [];
    this.page = 0;

    console.log(this.page+", "+this.search+", "+this.filterStock, 'FILTER')
    this.loadMore();
  }


  async getData(): Promise<any>{
    const produkList = await this.apiService.getProductList(this.page);
    this.listProduk = produkList;
    return produkList;
  }
  
  clearSearch(){
    this.search = "";
  }

  async loadMore(ev=null): Promise<any>{
    this.page++;
    this.apiService.getProductList(this.page, this.ctgId, this.attrString, this.search).then((res) => {
      this.dataFilter = res;
      if(this.filterStock == 'ready'){
        this.dataFilter = this.dataFilter.filter((val) =>{
          return (val.stock_quantity > 3);
        })
      }else if(this.filterStock == 'ganjil'){
        this.dataFilter = this.dataFilter.filter((val) => {
          return (val.stock_quantity > 0 && val.stock_quantity < 4);
        })
      }else if(this.filterStock == 'kosong'){
        this.dataFilter = this.dataFilter.filter((val) => {
          return (val.stock_quantity < 1);
        })
      }

      //@ts-ignore
      this.listProduk = [...this.listProduk, ...this.dataFilter];
      if(this.dataFilter.length < 7){
        this.loadMore();
      }
      if(ev != null) ev.target.complete();
    });
    
  }


  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: FilterPage,
      cssClass: 'my-custom-modal-class',
      componentProps: {
        diameterVal: this.idFilter ? this.idFilter.diameter : '',
        etVal: this.idFilter ? this.idFilter.et : '',
        spekVal: this.idFilter ? this.idFilter.spek : '',
        tireSpekVal: this.idFilter ? this.idFilter.tireSpek : '',
        widthVal: this.idFilter ? this.idFilter.width : '',
        qtyVal : this.filterStock ? this.filterStock : '',
      }
    });
    modal.onDidDismiss().then((res) => {
      let arr = [];
      this.attrString = "";
      if(res.data != undefined){
        this.filterStock = res.data.filterQty ? res.data.filterQty : '';
        this.idFilter = {
          diameter: res.data.filterDiameter ? res.data.filterDiameter : '',
          et: res.data.filterEt ? res.data.filterEt : '',
          spek: res.data.filterSpesifikasi ? res.data.filterSpesifikasi : '',
          tireSpek: res.data.filterTireSpek ? res.data.filterTireSpek : '',
          width: res.data.filterWidth ? res.data.filterWidth : ''
        }
        this.diameterArr = res.data.filterDiameter != undefined ? res.data.filterDiameter.slug + ':'+ res.data.filterDiameter.id  : null;
        this.etArr = res.data.filterEt != undefined ? res.data.filterEt.slug + ":" +res.data.filterEt.id : null;
        this.spesifikasiArr = res.data.filterSpesifikasi != undefined ? res.data.filterSpesifikasi.slug + ":" + res.data.filterSpesifikasi.id : null;
        this.tireSpekArr = res.data.filterTireSpek != undefined ? res.data.filterTireSpek.slug + ":" + res.data.filterTireSpek.id : null;
        this.widthArr = res.data.filterWidth != undefined ? res.data.filterWidth.slug + ":" + res.data.filterWidth.id : null;
        
        arr = [this.diameterArr, this.etArr, this.spesifikasiArr, this.tireSpekArr, this.widthArr];
        arr.forEach((val) => {
          if(val != null){
            if(this.attrString != ""){
              this.attrString += ",";
              this.attrString += val;
            }else{
              this.attrString += val;
            }
          }
        });
        this.reload();
      }else{
        this.filterStock = '';
        this.idFilter = false;
        this.reload();
      }
    })
    return await modal.present();
  }

  
  async modalDetail(idx : number){
    const modal= await this.modalCtrl.create({
      component: DetailPage,
      componentProps: {
        productDetail: this.listProduk[idx]
      }
    });
    return await modal.present();
  }

  mainMenu() {
    this.menu.open('main-menu');
  }
}
