import { Component } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage {
  data: any;
  constructor(
    public params : NavParams,
    public modalCtrl : ModalController
    ){ 
    this.data = params.data.productDetail;
    console.log(this.data)
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

}
