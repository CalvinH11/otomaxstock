import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Platform , NavController , MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  loginUser: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public storage : Storage,
    public navCtrl : NavController,
    public menu : MenuController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.cekLogin();
    });
  }

  cekLogin(){
    this.storage.get('login_id').then((val) => {
      if(val != null){
        this.loginUser = val;
        this.navCtrl.navigateRoot('home');
      }else{
        this.navCtrl.navigateRoot('login');
      }
    })
  }

  logout(){
    this.menu.close();
    this.storage.remove('login_id');
    this.navCtrl.navigateRoot('login');
  }
}
